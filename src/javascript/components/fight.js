import {controls,  CONTROL_KEYS} from "../../constants/controls";
import { TYPES_ATTACK } from "../../constants/attack";
import { NEED_TO_CRIT} from '../../constants/crit';
import { createFighterConfigs } from "./viewUpdate";
import { FIGHTER_POSITION } from '../../constants/positions';


export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const firstFighterLeft = createFighter(
      firstFighter,
      createFighterConfigs(FIGHTER_POSITION.LEFT),
    );
    
    const secondFighterRight = createFighter(
      secondFighter,
      createFighterConfigs(FIGHTER_POSITION.RIGHT),
    );

    firstFighterLeft.restartCrit();
    secondFighterRight.restartCrit();

    const pressedKeys = new Map();

    document.addEventListener('keydown', (e) => {
      if (e.repeat || !CONTROL_KEYS.some(key => key === e.code)) return;

      pressedKeys.set(e.code, true);

      fightProcess(firstFighterLeft, secondFighterRight, pressedKeys, e.code);

      if(firstFighterLeft.healthFighter <= 0) {
        resolve(secondFighter);
      } else if (secondFighterRight.healthFighter <= 0) {
        resolve(firstFighter);
      }
    });

    document.addEventListener('keyup', (e) => {
      if (e.code === controls.PlayerOneBlock) {
        firstFighterLeft.blockingAttack(false);
      }
      if (e.code === controls.PlayerTwoBlock) {
        secondFighterRight.blockingAttack(false);
      }
      pressedKeys.delete(e.code);
    });
  });
}

function createFighter(fighter, configs) {
  const { onPointsUpdated, blockingChanged, damageReceived, onAttacking } = configs;

  return {
    ...fighter,
    healthFighter : fighter.health,
    critPointsFighter : 0,
    isBlocking : false,
    timerId : null,

    receiveDamage(value) {
      this.healthFighter -= value;
      damageReceived(this.healthFighter, this.health);
    },

    blockingAttack(value) {
      this.isBlocking = value;
      blockingChanged(value);
    },

    doAttack(defender, damage) {
      defender.receiveDamage(damage);
      onAttacking(TYPES_ATTACK.PUNCH);
    },
    doCritAttack(defender) {
      if (!this.isDoCrit()) return;

      this.restartCrit();
      defender.receiveDamage(this.attack * 2);
      onAttacking(TYPES_ATTACK.FIREBALL);
    },

    isDoCrit() {
      return this.critPointsFighter === NEED_TO_CRIT;
    },

    restartCrit() {
      this.critPointsFighter = 0;
      onPointsUpdated(this.critPointsFighter, false);

      this.timerId = setInterval(() => {
        this.critPointsFighter++;

        const canDoCrit = this.isDoCrit();

        onPointsUpdated(this.critPointsFighter, canDoCrit);

        if (canDoCrit) {
          clearInterval(this.timerId);
        }
      }, 1000);
    },
  };
}

  function fightProcess(leftFighter, rightFighter, keyMap, code) {
    if(code === controls.PlayerOneBlock) {
      leftFighter.blockingAttack(true);
    }

    if(code === controls.PlayerTwoBlock) {
      rightFighter.blockingAttack(true);
    }

    if(code === controls.PlayerOneAttack) {
      doFighterAttack(leftFighter, rightFighter, keyMap);
      return;
    }

    if (code === controls.PlayerTwoAttack) {
      doFighterAttack(rightFighter, leftFighter, keyMap);
      return;
    }
    if (controls.PlayerOneCriticalHitCombination.every(code => keyMap.has(code))) {
      leftFighter.doCritAttack(rightFighter);
      return;
    }
    if (controls.PlayerTwoCriticalHitCombination.every(code => keyMap.has(code))) {
      rightFighter.doCritAttack(leftFighter);
    }
  }

  function doFighterAttack(attacker, defender) {
    if (attacker.isBlocking) {
      return;
    }

    if (defender.isBlocking) {
      attacker.doAttack(defender, 0);
      return;
    }

    attacker.doAttack(defender, getDamage(attacker, defender));
  }


export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  const hitPower = getRandomChance(1, 2);
  return fighter.attack * hitPower;
}

export function getBlockPower(fighter) {
  const blockChance = getRandomChance(1, 2);
  return fighter.defense * blockChance;
}

export function getRandomChance(min, max) {
  return Math.random() * (max - min) + min;
}
