import { createFighterImage } from "../fighterPreview";
import { showModal } from './modal';
 
export function showWinnerModal(fighter) {
  const imagesElement = createFighterImage(fighter);
  const modal = {
    title: `${fighter.name.toUpperCase()} winner!!`,
    bodyElement: imagesElement,
    onclose: () => {
      location.reload();
    }
  };
  showModal(modal);
}